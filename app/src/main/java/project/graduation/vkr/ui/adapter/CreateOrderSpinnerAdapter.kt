package project.graduation.vkr.ui.adapter

import android.R.layout.*
import android.content.Context
import project.graduation.vkr.model.data.table.OrganizationEntity

/**
 * Адаптер для представления и заполнения [android.widget.Spinner]
 *
 * Элементы спиннера [OrganizationEntity]
 * */
class CreateOrderSpinnerAdapter(ctx: Context, organizations: List<OrganizationEntity>) : AdapterWrapper<String>(ctx, android.R.layout.simple_spinner_item) {

    init {
        adapter.setDropDownViewResource(simple_spinner_item)
        adapter.addAll(organizations.map { it.name }.toList())
    }

}