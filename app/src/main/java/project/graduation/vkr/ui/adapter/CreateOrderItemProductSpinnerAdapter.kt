package project.graduation.vkr.ui.adapter

import android.content.Context
import project.graduation.vkr.model.data.table.ProductEntity

/**
 * Адаптер для представления и заполнения [android.widget.Spinner]
 *
 * Элементы спиннера [ProductEntity]
 * */
class CreateOrderItemProductSpinnerAdapter(ctx: Context, products: List<ProductEntity>) : AdapterWrapper<String>(ctx, android.R.layout.simple_spinner_item) {

    init {
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter.addAll(products.map { "Название: ${it.name} Арт. ${it.article}" })
    }

}