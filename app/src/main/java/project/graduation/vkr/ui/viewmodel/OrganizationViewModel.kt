package project.graduation.vkr.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.ViewModel
import project.graduation.vkr.model.data.table.OrganizationEntity
import project.graduation.vkr.ui.activity.App
import project.graduation.vkr.ui.async.Tasks

/**
 * Хранит и отдает данные о [OrganizationEntity]
 *
 * @see MediatorLiveData
 * @see Tasks.GetAllOrganizationsTask
 * @see project.graduation.vkr.model.data.repository.OrganizationRepo
 * */
class OrganizationViewModel : ViewModel() {
    var data: LiveData<List<OrganizationEntity>>

    init {
        val liveData = MediatorLiveData<List<OrganizationEntity>>()
        liveData.value = Tasks.GetAllOrganizationsTask().execute().get()
        liveData.addSource(App.database.orgRepo().elemsLD(), { liveData.value = it } )
        data = liveData
    }


}