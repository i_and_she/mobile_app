package project.graduation.vkr.ui.layout

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import project.graduation.vkr.R

/**
 * Разметка или Layout отвечающая за удобный доступ к вложенным [android.view.ViewGroup]
 *
 * Используется на экране деталей заказа
 * */
class OrderDetailsLayout(val layout: LinearLayout) {
    constructor(ctx: Context) : this(LayoutInflater.from(ctx).inflate(R.layout.order_details, null, false) as LinearLayout)

    val orderIdTxt = layout.findViewById(R.id.details_orderid) as TextView
    val orgNameTxt = layout.findViewById(R.id.details_org_name) as TextView
    val orgAddressTxt = layout.findViewById(R.id.details_org_address) as TextView
    val createdAtTxt = layout.findViewById(R.id.details_createdat) as TextView
    val elementsRV = layout.findViewById(R.id.details_orderitems) as RecyclerView

}