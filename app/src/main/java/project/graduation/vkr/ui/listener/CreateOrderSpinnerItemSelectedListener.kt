package project.graduation.vkr.ui.listener

import android.arch.lifecycle.MutableLiveData
import android.view.View
import android.widget.AdapterView
import project.graduation.vkr.model.data.table.OrganizationEntity

/**
 * Листенер слушающий нажатия на элемент из выпадающего списка организаций на экране создания заказа
 *
 * Меняет значение [MutableLiveData] позволяя зависимому классу реагировать на действия пользователя
 * */
class CreateOrderSpinnerItemSelectedListener(val itemMap: List<OrganizationEntity>, val chosenOrgLD: MutableLiveData<OrganizationEntity>)
    : AdapterView.OnItemSelectedListener
{
    val listAsMapIndexed: MutableMap<Int, OrganizationEntity> = HashMap()

    init {
        itemMap.forEachIndexed({ i, org -> listAsMapIndexed[i] = org })
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        chosenOrgLD.value = itemMap[position]
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        chosenOrgLD.value = null
    }
}