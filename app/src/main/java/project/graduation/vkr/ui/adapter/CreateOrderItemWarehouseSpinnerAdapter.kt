package project.graduation.vkr.ui.adapter

import android.content.Context
import project.graduation.vkr.model.data.table.WarehouseEntity

/**
 * Адаптер для представления и заполнения [android.widget.Spinner]
 *
 * Элементы спиннера [WarehouseEntity]
 * */
class CreateOrderItemWarehouseSpinnerAdapter(ctx: Context, warehouseList: List<WarehouseEntity>) : AdapterWrapper<String>(ctx, android.R.layout.simple_spinner_item) {

    companion object {
        fun warehouseToString(warehouseList: List<WarehouseEntity>): List<String> {
            return warehouseList.map { "Склад: ${it.name} по адресу: ${it.address}" }
        }
    }

    init {
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter.addAll(warehouseToString(warehouseList))
    }

}