package project.graduation.vkr.ui.layout

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.Spinner
import project.graduation.vkr.R

/**
 * Разметка или Layout отвечающая за удобный доступ к вложенным [android.view.ViewGroup]
 *
 * Используется на экране создания заказа
 * */
class CreateOrderLayout(val layout: FrameLayout) {
    constructor(ctx: Context) : this(LayoutInflater.from(ctx).inflate(R.layout.create_order, null, false) as FrameLayout)

    val spinner = layout.findViewById(R.id.create_order_spinner) as Spinner
    val fab = layout.findViewById(R.id.create_order_additem_fab) as FloatingActionButton

}