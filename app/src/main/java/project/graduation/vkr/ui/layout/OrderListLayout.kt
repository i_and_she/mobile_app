package project.graduation.vkr.ui.layout

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import project.graduation.vkr.R

/**
 * Разметка или Layout отвечающая за удобный доступ к вложенным [ViewGroup]
 *
 * Используется на экране списка заказов
 * */
class OrderListLayout(val layout: FrameLayout) {
    constructor(ctx: Context, parent: ViewGroup?) : this(LayoutInflater.from(ctx).inflate(R.layout.order_list, parent, false) as FrameLayout)

    val fab = layout.findViewById(R.id.floatingActionButton) as FloatingActionButton
    val elementsRV = layout.findViewById(R.id.orders_rv) as RecyclerView

}