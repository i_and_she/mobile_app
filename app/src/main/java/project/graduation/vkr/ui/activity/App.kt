package project.graduation.vkr.ui.activity

import android.app.Application
import android.arch.persistence.room.Room
import android.os.AsyncTask
import android.util.Log
import project.graduation.vkr.Database
import project.graduation.vkr.model.data.table.*
import java.util.*

/**
 * Класс наследующий [Application], имеет тот же жизненный цикл, что и само приложение
 * Таким образом в классе типа Application возможно создавать инстансы объектов, которые будут жить на протяжении всего
 * приложения, а также это неявно делает их одиночками
 *
 * Идеально подходит под объекты предоставляющие единый доступ в базу данных [Database]
* */
class App : Application() {

    companion object {
        lateinit var database: Database
        val TAG = "database"
    }

    /**
     * Метод [onCreate] вызывается один раз за жизнь приложения
     * В этом методе безопасно создавать уникальные объекты, такие как [Database]
     *
     * Метод инициализирует базу данных [Room] с помощью вспомогательного статического метода
     * Так как эта база преимущественно нужна для тестирования, то в методе используется метод, создающий базу в памяти [Room.inMemoryDatabaseBuilder]
     * */
    override fun onCreate() {
        super.onCreate()
        database = Room.inMemoryDatabaseBuilder(applicationContext, Database::class.java)
            .build()
        FillDataWithDummies(database).execute()
    }

    /**
     * Метод [onTerminate] также вызывается единожды за жизнь приложения, непосредственно перед тем,
     * как операционная система уничтожит инстанс приложения
     *
     * Здесь происходит как правило высвобождение каких-нибудь ресурсов
     * */
    override fun onTerminate() {
        super.onTerminate()
        database.close()
    }

    /**
     * Класс, расширяющий [AsyncTask]
     * AsyncTask в понимании операционной системы задача, исполняемая не в главном потоке, а в специальном фоновом
     *
     * Здесь в методе [AsyncTask.doInBackground] происходит инициализация базы данных тестовыми данными
     * */
    class FillDataWithDummies(val db: Database) : AsyncTask<Void, Void, Unit>() {
        override fun doInBackground(vararg params: Void) {
            db.runInTransaction({
                /**
                * Заполняем базу данных тестовыми данными
                * Там, где orgs - создаются прототипы сущности Организация
                * Где orders - прототипы сущности Заказ
                * И так далее
                *
                * Обратить внимание, что в качестве id используется 0,
                * для базы данных [Room] это индикатор того, что база должна сгенерировать этот id
                * */

                val orgs = List(
                    5,
                    { i ->
                        OrganizationEntity(
                            0,
                            "Рога и копыта №${i + 1}",
                            "Адрес №${i + 1}")
                    }
                ).toTypedArray()


                Log.i(TAG, "Orgs: ${Arrays.toString(orgs)}")

                val orders = List(
                    20,
                    { i ->
                        OrderEntity(
                            0,
                            (i % orgs.size).toLong() + 1L,
                            System.currentTimeMillis(),
                            OrderEntity.STANDBY
                        )
                    }
                ).toTypedArray()

                Log.i(TAG, "Orders: ${Arrays.toString(orders)}")

                val warehouses = List(
                    4,
                    {
                        WarehouseEntity(0, "Warehouse #$it", "Address #$it")
                    }
                ).toTypedArray()

                Log.i(TAG, "Warehouses: ${Arrays.toString(warehouses)}")

                val products = List(
                    2,
                    {
                        ProductEntity(0, "Product #$it", "Article of product #$it", "Description")
                    }
                ).toTypedArray()

                Log.i(TAG, "Products: ${Arrays.toString(products)}")

                val remainProd1Wh1 = WhRemainEntity(
                    1,
                    1,
                    5
                )
                val remainProd1Wh2 = WhRemainEntity(
                    2,
                    1,
                    6
                )
                val remainProd2Wh3 = WhRemainEntity(
                    3,
                    2,
                    7
                )
                val remainProd2Wh4 = WhRemainEntity(
                    4,
                    2,
                    8
                )

                /**
                *
                * Далее необходимо все эти прототипы сохранить в БД, с помощью методов [project.graduation.vkr.model.data.repository.GenericRepository.insert]
                * */

                db.orgRepo().insert(orgs)
                db.orderRepo().insert(orders)
                db.warehouseRepo().insert(warehouses)
                db.productRepo().insert(products)
                db.whRemainRepo().insert(arrayOf(remainProd1Wh1, remainProd1Wh2, remainProd2Wh3, remainProd2Wh4))

//                db.orderItemRepo().insert(orderItems)

                Log.i(TAG, "db populated")
            })
        }

    }
}
