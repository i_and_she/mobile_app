package project.graduation.vkr.ui.activity

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import project.graduation.vkr.model.data.table.ProductEntity
import project.graduation.vkr.model.data.table.WarehouseEntity
import project.graduation.vkr.model.data.table.WhRemainEntity
import project.graduation.vkr.ui.adapter.AdapterWrapper
import project.graduation.vkr.ui.adapter.CreateOrderItemProductSpinnerAdapter
import project.graduation.vkr.ui.adapter.CreateOrderItemQuantitySpinnerAdapter
import project.graduation.vkr.ui.adapter.CreateOrderItemWarehouseSpinnerAdapter
import project.graduation.vkr.ui.async.Tasks
import project.graduation.vkr.ui.layout.CreateOrderItemLayout
import project.graduation.vkr.ui.listener.CreateOrderItemFABClickListener
import project.graduation.vkr.ui.listener.ProductSpinnerItemSelectedListener
import project.graduation.vkr.ui.listener.QuantitySpinnerItemSelectedListener
import project.graduation.vkr.ui.listener.WarehouseSpinnerItemSelectedListener
import java.util.*
import kotlin.collections.HashMap

/**
 * Класс расширяющий [AppCompatActivity]
 *
 * Отвечает за отрисовку и расстановку коллбэков на экране добавления элемента заказа
 *
 * @see CreateOrderItemLayout
 * @see Tasks.GetAllProductsTask
 * @see CreateOrderItemWarehouseSpinnerAdapter
 * @see CreateOrderItemProductSpinnerAdapter
 * @see CreateOrderItemQuantitySpinnerAdapter
 * @see WarehouseSpinnerItemSelectedListener
 * @see ProductSpinnerItemSelectedListener
 * */
class CreateOrderItemActivity : AppCompatActivity() {
    companion object {
        val TAG = "CreateOrderItemActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutWrapper = CreateOrderItemLayout(this, null)

        val productsLD = Tasks.GetAllProductsTask().execute().get()
        val productsAsMap = HashMap<Int, ProductEntity>()
        productsAsMap.putAll(productsLD.indices.associateBy({ it }, { productsLD[it] }))

        val productLiveData = MutableLiveData<ProductEntity>()
        val warehouseLiveData = MutableLiveData<WarehouseEntity>()
        val quantityLiveData = MutableLiveData<Int>()

        val whRemainsList = mutableListOf<WhRemainEntity>()
        val warehouseMap = HashMap<Int, WarehouseEntity>()
        productLiveData.observe(this, Observer { product ->
            Log.i(TAG, "Product LiveData changed")
            if (product != null) {
                whRemainsList.clear()
                whRemainsList.addAll(Tasks.GetAllWhRemainsByProductTask(product.id).execute().get())
                val whList = Tasks.GetAllWarehousesInList(whRemainsList.map { it.wh_id }).execute().get()
                warehouseMap.clear()
                warehouseMap.putAll(whList.indices.associateBy({it}, { whList[it] }))
                layoutWrapper.warehouseSpinner.adapter = CreateOrderItemWarehouseSpinnerAdapter(this, whList)
            }
        })

        warehouseLiveData.observe(this, Observer { it ->
            Log.i(TAG, "Warehouse LiveData changed")
            val product = productLiveData.value
            val warehouse = it
            if (product != null && warehouse != null) {
                val whRemain = whRemainsList.find { it.vendible_id == product.id && it.wh_id == warehouse.id }

                if (whRemain != null) {
                    val realAdapter : ArrayAdapter<String> = (layoutWrapper.quantitySpinner.adapter as AdapterWrapper<String>).adapter
                    realAdapter.clear()
                    realAdapter.addAll(IntRange(1, whRemain.quantity).toList().map { it.toString() })
                }
            } else {
                Log.i(TAG, "WhRemain not found")
                Log.i(TAG, "Current product: $product; Current warehouse: $warehouse")
            }
        })

        layoutWrapper.productSpinner.adapter = CreateOrderItemProductSpinnerAdapter(this, productsLD)
        layoutWrapper.productSpinner.onItemSelectedListener = ProductSpinnerItemSelectedListener(productsAsMap, productLiveData)

        layoutWrapper.warehouseSpinner.adapter = CreateOrderItemWarehouseSpinnerAdapter(this, Collections.emptyList())
        layoutWrapper.warehouseSpinner.onItemSelectedListener = WarehouseSpinnerItemSelectedListener(warehouseMap, warehouseLiveData)

        layoutWrapper.quantitySpinner.adapter = CreateOrderItemQuantitySpinnerAdapter(this, Collections.emptyList())
        layoutWrapper.quantitySpinner.onItemSelectedListener = QuantitySpinnerItemSelectedListener(quantityLiveData)

        layoutWrapper.doneFab.setOnClickListener(CreateOrderItemFABClickListener(this, productLiveData, warehouseLiveData, quantityLiveData.value ?: 0))

        setContentView(layoutWrapper.layout)
    }


}