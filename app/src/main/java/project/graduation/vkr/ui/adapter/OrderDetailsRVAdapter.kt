package project.graduation.vkr.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.LinearLayout
import project.graduation.vkr.ui.layout.OrderDetailsItemLayout
import project.graduation.vkr.ui.viewholder.GenericViewHolder
import project.graduation.vkr.ui.viewmodel.OrderViewModel

/**
 * Адаптер для [RecyclerView] на экране деталей заказа (список товаров в корзине)
 *
 * Предоставляет шаблон представления, который заполняется из данных [OrderViewModel.OrderDetailedView]
 *
 * @see RecyclerView
 * @see OrderDetailsItemLayout
 * @see GenericViewHolder
 * @see OrderDetailsItemLayout
 * */
class OrderDetailsRVAdapter(val order: List<OrderViewModel.OrderItemDetailedView>) : RecyclerView.Adapter<GenericViewHolder>() {
    lateinit var context: Context

    override fun getItemCount(): Int {
        return order.size
    }

    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
        val layout = OrderDetailsItemLayout(holder.viewGroup as LinearLayout)

        layout.warehouseNameTxt.text = order[position].warehouseName
        layout.productNameTxt.text = order[position].productName
        layout.productArticleTxt.text = order[position].productArticle
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        context = parent.context

        val view = OrderDetailsItemLayout(context, parent)

        return GenericViewHolder(view.layout)
    }

}