package project.graduation.vkr.ui.async

import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import android.util.Log
import project.graduation.vkr.model.data.table.*
import project.graduation.vkr.ui.activity.App
import project.graduation.vkr.ui.viewmodel.OrderViewModel
import java.sql.Timestamp
import java.util.concurrent.Callable

/**
 * Таск или задача в андроиде исполняется операционной системой в отдельном потоке, причем
 * на старых версиях для тасков использовался один и тот же поток, что делало их исполнение последовательным
 *
 * Асинхронные таски необходимы для любых задач ввода/вывода или иначе говоря - блокирующих поток исполнения
 *
 * В модели андроида существует главный поток приложения или UI поток, который всегда должен отвечать за
 * отображение интерфейса и взаимодействие с ним пользователя, и поэтому если осуществляется блокирующая поток операция,
 * то страдает отрисовка экрана, что может вынудить операционную систему удалить приложение из памяти
 *
 * @see AsyncTask
 * */
class Tasks {

    /**
     * Получить все заказы
     *
     * @return List<OrderEntity>
     * */
    class GetAllOrdersTask : AsyncTask<Void, Void, List<OrderEntity>>() {
        override fun doInBackground(vararg params: Void?): List<OrderEntity> {
            return App.database.orderRepo().ordersList()
        }
    }

    /**
     * Получить все продукты (товары)
     *
     * @return List<ProductEntity>
     * */
    class GetAllProductsTask : AsyncTask<Void, Void, List<ProductEntity>>() {
        override fun doInBackground(vararg params: Void?): List<ProductEntity> {
            return App.database.productRepo().elems()
        }
    }

    /**
     * Получить [OrderViewModel.OrderDetailedView] представление заказа
     *
     * @return OrderViewModel.OrderDetailedView
     * */
    class GetDetailedViewTask(val orderId: Long, val ordersLD: LiveData<List<OrderEntity>>) : AsyncTask<Void, Void, OrderViewModel.OrderDetailedView>() {
        override fun doInBackground(vararg params: Void?): OrderViewModel.OrderDetailedView {
            return App.database.runInTransaction(Callable<OrderViewModel.OrderDetailedView> {
                val order = ordersLD.value.orEmpty().first { it.id == orderId }

                val org = App.database.orgRepo().elems().first { it.id == order.org_id }

                val orderItems = App.database.orderItemRepo().getByOrder(orderId)

                val warehouses = App.database.warehouseRepo().findWithIdInList(orderItems.map { it.wh_id }.distinct().toList())

                val products = App.database.productRepo().findWithIdInList(orderItems.map { it.vendible_id }.distinct().toList())

                val orderItemViewList = List(orderItems.size, {
                    val orderItem = orderItems[it]
                    val wh = warehouses.filter { it.id == orderItem.wh_id }.first()
                    val product = products.filter { it.id == orderItem.vendible_id }.first()
                    OrderViewModel.OrderItemDetailedView(wh.name, product.name, product.article)
                })

                OrderViewModel.OrderDetailedView(0, org.name, org.address, Timestamp(order.created_at), orderItemViewList)
            })
        }
    }

    /**
     * Получить все организации
     *
     * @return List<OrganizationEntity>
     * */
    class GetAllOrganizationsTask : AsyncTask<Void, Void, List<OrganizationEntity>>() {
        override fun doInBackground(vararg params: Void?): List<OrganizationEntity> {
            return App.database.orgRepo().elems()
        }
    }

    /**
     * Получить все остатки продукта с [id] на складах
     *
     * @return List<WhRemainEntity>
     * */
    class GetAllWhRemainsByProductTask(val id: Long) : AsyncTask<Void, Void, List<WhRemainEntity>>() {

        override fun doInBackground(vararg params: Void?): List<WhRemainEntity> {
            return App.database.whRemainRepo().findByProductId(id)
        }
    }

    /**
     * Получить все склады, чей [WarehouseEntity.id] в списке [ids]
     *
     * @return List<WarehouseEntity>
     * */
    class GetAllWarehousesInList(val ids: List<Long>) : AsyncTask<Void, Void, List<WarehouseEntity>>() {

        override fun doInBackground(vararg params: Void?): List<WarehouseEntity> {
            return App.database.warehouseRepo().findWithIdInList(ids)
        }

    }

    /**
     * Добавить или заменить остаток товара на складе [WhRemainEntity]
     *
     * @see project.graduation.vkr.model.data.repository.WhRemainRepo
     * @return Unit
     * */
    class InsertOrReplaceWhRemainTask(val whRemainEntity: WhRemainEntity) : AsyncTask<Void, Void, Unit>() {
        override fun doInBackground(vararg params: Void?) {
            App.database.whRemainRepo().insertOrReplace(whRemainEntity)
        }

    }

}