package project.graduation.vkr.ui.layout

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Spinner
import project.graduation.vkr.R

/**
 * Разметка или Layout отвечающая за удобный доступ к вложенным [ViewGroup]
 *
 * Используется на экране создания элемента заказа
 * */
class CreateOrderItemLayout(val layout: FrameLayout) {
    companion object {
        const val spinnersLayoutId = R.id.create_orderitem_spinner_container
        const val warehouseId = R.id.create_orderitem_wh_spinner
        const val productId = R.id.create_orderitem_product_spinner
        const val quantityId = R.id.create_orderitem_quantity_spinner
        const val doneFabId = R.id.create_orderitem_done_fab
    }

    constructor(ctx: Context, parent: ViewGroup?) : this(LayoutInflater.from(ctx).inflate(R.layout.create_orderitem, parent, false) as FrameLayout)

    val warehouseSpinner = layout.findViewById(warehouseId) as Spinner
    val productSpinner = layout.findViewById(productId) as Spinner
    val quantitySpinner = layout.findViewById(quantityId) as Spinner
    val spinnersContainer = layout.findViewById(spinnersLayoutId) as LinearLayout
    val doneFab = layout.findViewById(doneFabId) as FloatingActionButton

}