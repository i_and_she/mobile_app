package project.graduation.vkr.ui.activity

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import project.graduation.vkr.model.data.table.OrganizationEntity
import project.graduation.vkr.ui.adapter.CreateOrderSpinnerAdapter
import project.graduation.vkr.ui.layout.CreateOrderLayout
import project.graduation.vkr.ui.listener.CreateOrderFABClickListener
import project.graduation.vkr.ui.listener.CreateOrderSpinnerItemSelectedListener
import project.graduation.vkr.ui.viewmodel.OrganizationViewModel


/**
 * Данная [AppCompatActivity] отвечает за отрисовку и установку коллбэков экрана создания заказа
 *
 * @see CreateOrderLayout
 * @see OrganizationViewModel
 * @see CreateOrderSpinnerItemSelectedListener
 * @see CreateOrderSpinnerAdapter
 * */
class CreateOrderActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layout = CreateOrderLayout(this)

        val orgViewModel = ViewModelProviders.of(this).get(OrganizationViewModel::class.java)

        val organizations = orgViewModel.data.value

        val organizationLiveData = MutableLiveData<OrganizationEntity>()
        layout.spinner.onItemSelectedListener = CreateOrderSpinnerItemSelectedListener(organizations.orEmpty(), organizationLiveData)
        layout.spinner.adapter = CreateOrderSpinnerAdapter(this, organizations.orEmpty())

        layout.fab.setOnClickListener(CreateOrderFABClickListener(this))

        setContentView(layout.layout)
    }

}