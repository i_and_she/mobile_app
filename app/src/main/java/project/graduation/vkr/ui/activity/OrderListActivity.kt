package project.graduation.vkr.ui.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import project.graduation.vkr.ui.adapter.OrderListRVAdapter
import project.graduation.vkr.ui.layout.OrderListLayout
import project.graduation.vkr.ui.listener.OrderListFABClickListener
import project.graduation.vkr.ui.viewmodel.OrderViewModel
import project.graduation.vkr.ui.viewmodel.OrganizationViewModel

/**
 * Класс, расширяющий [AppCompatActivity]
 *
 * Отвечает за отрисовку и расстановку коллбэков на экране списка заказов (главный экран)
 *
 * @see OrderListLayout
 * @see OrganizationViewModel
 * @see OrderViewModel
 * @see LinearLayoutManager
 * @see OrderListRVAdapter
 * @see OrderListFABClickListener
 * */
class OrderListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutObject = OrderListLayout(this, null)

        layoutObject.fab.setOnClickListener(OrderListFABClickListener(this))

        setContentView(layoutObject.layout)

        val orgs = ViewModelProviders.of(this).get(OrganizationViewModel::class.java).data
        val orders = ViewModelProviders.of(this).get(OrderViewModel::class.java).ordersLD

        val viewManager = LinearLayoutManager(this)
        val viewAdapter = OrderListRVAdapter(orders.value.orEmpty(), orgs.value.orEmpty())

        layoutObject.elementsRV.apply {

            setHasFixedSize(true)

            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

}
