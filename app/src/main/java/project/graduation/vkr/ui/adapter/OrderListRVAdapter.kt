package project.graduation.vkr.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.ocpsoft.prettytime.PrettyTime
import project.graduation.vkr.ui.activity.OrderDetailsActivity
import project.graduation.vkr.R
import project.graduation.vkr.model.data.table.OrderEntity
import project.graduation.vkr.model.data.table.OrganizationEntity
import project.graduation.vkr.ui.layout.OrderListItemLayout
import project.graduation.vkr.ui.viewholder.GenericViewHolder
import java.sql.Timestamp
import java.util.*

/**
 * Адаптер для [RecyclerView] на экране списка заказов
 *
 * Предоставляет шаблон представления, который заполняется из данных [OrganizationEntity] и [OrderEntity]
 *
 * Запускает [OrderDetailsActivity] по клику на элемент списка
 *
 * @see RecyclerView
 * @see GenericViewHolder
 * @see PrettyTime
 * @see OrderListItemLayout
 * @see View.OnClickListener
 * @see OrderDetailsActivity
 * */
class OrderListRVAdapter(val myDataset: List<OrderEntity>, val customers : List<OrganizationEntity>) : RecyclerView.Adapter<GenericViewHolder>(), View.OnClickListener {

    companion object {
        val TAG = "ordersAdapter"
    }

    lateinit var context : Context
    val timePretty = PrettyTime().setLocale(Locale("RU"))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        context = parent.context

        val textView = OrderListItemLayout(context, parent)

        textView.layout.setOnClickListener(this)

        return GenericViewHolder(textView.layout)
    }

    override fun onClick(v: View) {
        val intent = Intent(context, OrderDetailsActivity::class.java)

        val layout = v as LinearLayout

        val textView = layout.findViewById(R.id.orderlistitem_realid) as TextView
        val orderId = textView.text.toString().toLong()
        intent.putExtra("orderid", orderId)

        ContextCompat.startActivity(context, intent, null)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holderGeneric: GenericViewHolder, position: Int) {
        val order = myDataset[position]

        val layout = OrderListItemLayout(holderGeneric.viewGroup as LinearLayout)

        layout.orderRealId.text = order.id.toString()
        val org = customers.filter { customer -> customer.id == order.org_id }[0]
        layout.orgName.text = org.name
        layout.orderId.text = "Номер заказа: ${order.id}"
        val timestamp = Timestamp(order.created_at)
        layout.orderCreated.text = "Создан ${timePretty.format(timestamp)}"
    }

    override fun getItemCount() = myDataset.size

}
