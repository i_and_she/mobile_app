package project.graduation.vkr.ui.viewmodel

import android.arch.lifecycle.*
import project.graduation.vkr.model.data.table.OrderEntity
import project.graduation.vkr.ui.activity.App
import project.graduation.vkr.ui.async.Tasks
import java.sql.Timestamp

/**
 * Отвечает за хранение и выдачу данных в определенном представлении (в данном случае [OrderDetailedView] и [OrderItemDetailedView])
 *
 * Хранит [LiveData] для [OrderEntity]
 *
 * @see MediatorLiveData
 * @see project.graduation.vkr.model.data.repository.OrderRepo
 * @see Tasks.GetDetailedViewTask
 * */
class OrderViewModel : ViewModel() {
    companion object {
        private var ordersLD: LiveData<List<OrderEntity>> = MutableLiveData()
    }

    var ordersLD: LiveData<List<OrderEntity>>

    init {
        if (OrderViewModel.ordersLD.value == null) {
            val liveData = MediatorLiveData<List<OrderEntity>>()
            liveData.value = Tasks.GetAllOrdersTask().execute().get()
            liveData.addSource(App.database.orderRepo().orders(), { liveData.value = it })

            ordersLD = liveData
            OrderViewModel.ordersLD = liveData
        } else {
            ordersLD = OrderViewModel.ordersLD
        }
    }

    fun detailedView(orderId: Long): OrderDetailedView {
        return Tasks.GetDetailedViewTask(orderId, ordersLD).execute().get()
    }

    data class OrderDetailedView(
        val orderId: Long,
        val orgName: String,
        val orgAddress: String,
        val createdAt: Timestamp,
        val items: List<OrderItemDetailedView>
    )

    data class OrderItemDetailedView(
        val warehouseName: String,
        val productName: String,
        val productArticle: String
    )
}