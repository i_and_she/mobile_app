package project.graduation.vkr.ui.listener

import android.content.Context
import android.content.Intent
import android.view.View
import project.graduation.vkr.ui.activity.CreateOrderItemActivity

/**
 * Листенер, срабатывающий при клике на кнопку + на экране создания заказа
 *
 * Создает активити [CreateOrderItemActivity]
 * */
class CreateOrderFABClickListener(val ctx: Context) : View.OnClickListener {
    override fun onClick(v: View?) {
        val intent = Intent(ctx, CreateOrderItemActivity::class.java)
        ctx.startActivity(intent)
    }
}