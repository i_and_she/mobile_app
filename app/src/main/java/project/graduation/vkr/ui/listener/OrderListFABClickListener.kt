package project.graduation.vkr.ui.listener

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.view.View
import project.graduation.vkr.ui.activity.CreateOrderActivity

/**
 * Листенер, слушающий клик по кнопке + на экране списка заказов
 *
 * Создает активность [CreateOrderActivity]
 * */
class OrderListFABClickListener(val ctx: Context) : View.OnClickListener {

    override fun onClick(v: View) {
        val intent = Intent(ctx, CreateOrderActivity::class.java)
        ContextCompat.startActivity(ctx, intent, null)
    }

}