package project.graduation.vkr.ui.adapter

import android.content.Context
import android.database.DataSetObserver
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter

/**
 * Класс, прототип, является по факту оберткой для [ArrayAdapter]
 *
 * Основное назначение: избавить от необходимости писать шаблонный код в местах использования [SpinnerAdapter]
 * путем наследования от этого класса
 *
 * @see CreateOrderItemQuantitySpinnerAdapter
 * @see CreateOrderItemProductSpinnerAdapter
 * @see CreateOrderItemWarehouseSpinnerAdapter
 * @see CreateOrderSpinnerAdapter
 * */
open class AdapterWrapper<T>(ctx: Context, layoutId: Int) : SpinnerAdapter {
    val adapter = ArrayAdapter<T>(ctx, layoutId)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return adapter.getView(position, convertView, parent)
    }

    override fun registerDataSetObserver(observer: DataSetObserver?) {
        adapter.registerDataSetObserver(observer)
    }

    override fun getItemViewType(position: Int): Int {
        return adapter.getItemViewType(position)
    }

    override fun getItem(position: Int): Any {
        return adapter.getItem(position) as Any
    }

    override fun getViewTypeCount(): Int {
        return adapter.viewTypeCount
    }

    override fun getItemId(position: Int): Long {
        return adapter.getItemId(position)
    }

    override fun hasStableIds(): Boolean {
        return adapter.hasStableIds()
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return adapter.getDropDownView(position, convertView, parent)
    }

    override fun unregisterDataSetObserver(observer: DataSetObserver?) {
        adapter.unregisterDataSetObserver(observer)
    }

    override fun getCount(): Int {
        return adapter.count
    }

    override fun isEmpty(): Boolean {
        return adapter.isEmpty
    }
}