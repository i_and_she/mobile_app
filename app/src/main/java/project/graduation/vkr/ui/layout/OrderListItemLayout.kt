package project.graduation.vkr.ui.layout

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import project.graduation.vkr.R

/**
 * Разметка или Layout отвечающая за удобный доступ к вложенным [ViewGroup]
 *
 * Используется на экране списка заказов (один заказ)
 * */
class OrderListItemLayout(val layout: LinearLayout) {
    constructor(ctx: Context, parent: ViewGroup?) : this(LayoutInflater.from(ctx).inflate(R.layout.order_list_item, parent, false) as LinearLayout)

    val orgName: TextView = layout.findViewById(R.id.orderlistitem_organization)
    val orderCreated: TextView = layout.findViewById(R.id.orderlistitem_created)
    val orderId: TextView = layout.findViewById(R.id.orderlistitem_id)
    val orderRealId: TextView = layout.findViewById(R.id.orderlistitem_realid)
}