package project.graduation.vkr.ui.layout

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import project.graduation.vkr.R

/**
 * Разметка или Layout отвечающая за удобный доступ к вложенным [ViewGroup]
 *
 * Используется на экране деталей заказа (элемент заказа)
 * */
class OrderDetailsItemLayout(val layout: LinearLayout) {
    constructor(ctx: Context, parent: ViewGroup?) : this(LayoutInflater.from(ctx).inflate(R.layout.order_details_item, parent, false) as LinearLayout)

    val warehouseNameTxt = layout.findViewById(R.id.orderdetailsitem_wh_name) as TextView
    val productNameTxt = layout.findViewById(R.id.orderdetailsitem_product_name) as TextView
    val productArticleTxt = layout.findViewById(R.id.orderdetailsitem_product_article) as TextView
}