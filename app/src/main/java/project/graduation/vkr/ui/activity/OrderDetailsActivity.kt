package project.graduation.vkr.ui.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import project.graduation.vkr.ui.adapter.OrderDetailsRVAdapter
import project.graduation.vkr.ui.layout.OrderDetailsLayout
import project.graduation.vkr.ui.viewmodel.OrderViewModel

/**
 * Класс, расширяющий [AppCompatActivity]
 *
 * Отвечает за отрисовку и расстановку коллбэков на экране деталей заказа
 *
 * @see OrderDetailsLayout
 * @see OrderViewModel
 * @see OrderDetailsRVAdapter
 * @see LinearLayoutManager
 * */
class OrderDetailsActivity : AppCompatActivity() {

    companion object {
        val TAG = "orderDetailsActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val orderId = intent.getLongExtra("orderid", -1L)
        if (orderId == -1L) {
            throw IllegalStateException("Created activity with empty state!")
        }

        val layout = OrderDetailsLayout(this)

        val orders = ViewModelProviders.of(this).get(OrderViewModel::class.java)
        val order = orders.detailedView(orderId)

        layout.orderIdTxt.text = orderId.toString()
        layout.orgNameTxt.text = order.orgName
        layout.orgAddressTxt.text = order.orgAddress
        layout.createdAtTxt.text = order.createdAt.toString()

        setContentView(layout.layout)

        val viewManager = LinearLayoutManager(this)
        val viewAdapter = OrderDetailsRVAdapter(order.items)

        layout.elementsRV.apply {
            layoutManager = viewManager
            adapter = viewAdapter

            setHasFixedSize(true)
        }
    }

}