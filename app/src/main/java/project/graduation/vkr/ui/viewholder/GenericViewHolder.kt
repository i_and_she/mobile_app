package project.graduation.vkr.ui.viewholder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

class GenericViewHolder(val viewGroup: ViewGroup) : RecyclerView.ViewHolder(viewGroup)