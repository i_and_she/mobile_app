package project.graduation.vkr

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import project.graduation.vkr.model.data.repository.*
import project.graduation.vkr.model.data.table.*

@Database(
    entities = [
        ProductEntity::class,
        WarehouseEntity::class,
        WhRemainEntity::class,
        OrganizationEntity::class,
        OrderEntity::class,
        OrderItemEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class Database : RoomDatabase() {

        abstract fun orderRepo() : OrderRepo

        abstract fun orgRepo() : OrganizationRepo

        abstract fun productRepo() : ProductRepo

        abstract fun warehouseRepo() : WarehouseRepo

        abstract fun whRemainRepo() : WhRemainRepo

        abstract fun orderItemRepo() : OrderItemRepo

}