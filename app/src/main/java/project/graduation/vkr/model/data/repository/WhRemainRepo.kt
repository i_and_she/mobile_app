package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import project.graduation.vkr.model.data.table.WhRemainEntity
import project.graduation.vkr.model.data.table.WhRemainEntity.Companion.TABLE_NAME

@Dao
interface WhRemainRepo : GenericRepository<WhRemainEntity> {
    @Insert
    override fun insert(elems: Array<WhRemainEntity>): List<Long>

    @Update
    override fun update(elems: Array<WhRemainEntity>): Int

    @Query("select * from $TABLE_NAME")
    override fun elemsLD(): LiveData<List<WhRemainEntity>>

    @Query("select * from $TABLE_NAME")
    override fun elems(): List<WhRemainEntity>

    @Query("select * from $TABLE_NAME where vendible_id = :id")
    fun findByProductId(id: Long) : List<WhRemainEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrReplace(whRemain: WhRemainEntity)

}