package project.graduation.vkr.model.data.table

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import project.graduation.vkr.model.data.table.OrderItemEntity.Companion.ORDER_ITEM

@Entity(
    tableName = ORDER_ITEM,
    foreignKeys = [
        (ForeignKey(
        entity = WhRemainEntity::class,
        parentColumns = ["wh_id", "vendible_id"],
        childColumns = ["wh_id", "vendible_id"],
        onDelete = ForeignKey.RESTRICT,
        onUpdate = ForeignKey.RESTRICT
    )),
        (ForeignKey(
        entity = OrderEntity::class,
        parentColumns = ["id"],
        childColumns = ["order_id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    ))
    ]
)
data class OrderItemEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var order_id: Long,
    var wh_id: Long,
    var vendible_id: Long,
    var quantity: Int
) {
    companion object {
        const val ORDER_ITEM = "order_items"
    }
}