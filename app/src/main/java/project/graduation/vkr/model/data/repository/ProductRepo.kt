package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import project.graduation.vkr.model.data.table.ProductEntity
import project.graduation.vkr.model.data.table.ProductEntity.Companion.TABLE_NAME

@Dao
interface ProductRepo : SimpleRepository<ProductEntity> {
    @Insert
    override fun insert(elems: Array<ProductEntity>): List<Long>

    @Update
    override fun update(elems: Array<ProductEntity>): Int

    @Query("select * from $TABLE_NAME")
    override fun elemsLD(): LiveData<List<ProductEntity>>

    @Query("select * from $TABLE_NAME where id = :id")
    override fun elem(id: Long): ProductEntity

    @Query("select * from $TABLE_NAME")
    override fun elems(): List<ProductEntity>

    @Query("select * from $TABLE_NAME where id in (:ids)")
    fun findWithIdInList(ids : List<Long>): List<ProductEntity>
}