package project.graduation.vkr.model.data.table

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import project.graduation.vkr.model.data.table.OrderEntity.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = OrganizationEntity::class,
            parentColumns = ["id"],
            childColumns = ["org_id"],
            onDelete = ForeignKey.RESTRICT,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class OrderEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var org_id: Long,
    var created_at: Long = System.currentTimeMillis(),
    var status: Int = STANDBY
) {
    companion object {
        const val TABLE_NAME = "orders"
        val STANDBY = 1
        val SHIPPED = 2
        val REJECTED = 3
        val PROCESSING = 4
    }

}