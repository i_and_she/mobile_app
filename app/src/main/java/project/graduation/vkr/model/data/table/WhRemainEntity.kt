package project.graduation.vkr.model.data.table

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import project.graduation.vkr.model.data.table.WhRemainEntity.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [
        (ForeignKey(
        entity = WarehouseEntity::class,
        parentColumns = ["id"],
        childColumns = ["wh_id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )),
        (ForeignKey(
        entity = ProductEntity::class,
        parentColumns = ["id"],
        childColumns = ["vendible_id"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    ))
    ],
    primaryKeys = ["wh_id", "vendible_id"])
data class WhRemainEntity(
    var wh_id : Long = 0,
    var vendible_id : Long = 0,
    var quantity : Int = 0
) {
    companion object {
        const val TABLE_NAME = "wh_remains"
    }
}