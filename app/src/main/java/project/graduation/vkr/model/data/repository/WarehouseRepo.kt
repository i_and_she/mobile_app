package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import project.graduation.vkr.model.data.table.WarehouseEntity
import project.graduation.vkr.model.data.table.WarehouseEntity.Companion.TABLE_NAME

@Dao
interface WarehouseRepo : SimpleRepository<WarehouseEntity> {
    @Insert
    override fun insert(elems: Array<WarehouseEntity>): List<Long>

    @Update
    override fun update(elems: Array<WarehouseEntity>): Int

    @Query("select * from $TABLE_NAME where id = :id")
    override fun elem(id: Long): WarehouseEntity

    @Query("select * from $TABLE_NAME")
    override fun elemsLD(): LiveData<List<WarehouseEntity>>

    @Query("select * from $TABLE_NAME")
    override fun elems(): List<WarehouseEntity>

    @Query("select * from $TABLE_NAME where id IN (:ids)")
    fun findWithIdInList(ids: List<Long>) : List<WarehouseEntity>
}