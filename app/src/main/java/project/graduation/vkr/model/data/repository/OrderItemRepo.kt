package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import project.graduation.vkr.model.data.table.OrderItemEntity
import project.graduation.vkr.model.data.table.OrderItemEntity.Companion.ORDER_ITEM

@Dao
interface OrderItemRepo : SimpleRepository<OrderItemEntity> {
    @Insert
    override fun insert(elems: Array<OrderItemEntity>): List<Long>

    @Update
    override fun update(elems: Array<OrderItemEntity>): Int

    @Query("select * from $ORDER_ITEM where id = :id")
    override fun elem(id: Long): OrderItemEntity

    @Query("select * from $ORDER_ITEM")
    override fun elemsLD(): LiveData<List<OrderItemEntity>>

    @Query("select * from $ORDER_ITEM")
    override fun elems(): List<OrderItemEntity>

    @Query("select * from $ORDER_ITEM where order_id = :id")
    fun getByOrder(id: Long): List<OrderItemEntity>

}