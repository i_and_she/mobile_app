package project.graduation.vkr.model.data.repository

interface SimpleRepository<T> : GenericRepository<T> {
    fun elem(id: Long): T
}