package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData

interface GenericRepository<T> {

    fun insert(elems: Array<T>): List<Long>

    fun update(elems: Array<T>): Int

    fun elemsLD(): LiveData<List<T>>

    fun elems(): List<T>

}