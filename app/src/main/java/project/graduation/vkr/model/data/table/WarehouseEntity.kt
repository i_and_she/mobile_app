package project.graduation.vkr.model.data.table

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import project.graduation.vkr.model.data.table.WarehouseEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class WarehouseEntity(
    @PrimaryKey(autoGenerate = true)
    var id : Long = 0,
    var name : String,
    var address : String
) {
    companion object {
        const val TABLE_NAME = "warehouses"
    }
}