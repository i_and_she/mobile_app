package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import project.graduation.vkr.model.data.table.OrderEntity
import project.graduation.vkr.model.data.table.OrderEntity.Companion.TABLE_NAME

@Dao
interface OrderRepo : SimpleRepository<OrderEntity> {
    @Update
    override fun update(elems: Array<OrderEntity>): Int

    @Query("select * from $TABLE_NAME")
    override fun elemsLD(): LiveData<List<OrderEntity>>

    @Query("select * from $TABLE_NAME")
    override fun elems(): List<OrderEntity>

    @Insert
    override fun insert(elems: Array<OrderEntity>): List<Long>

    @Query("select * from $TABLE_NAME where id = :id")
    override fun elem(id: Long): OrderEntity

    @Query("select * from $TABLE_NAME")
    fun orders(): LiveData<List<OrderEntity>>

    @Query("select * from $TABLE_NAME")
    fun ordersList(): List<OrderEntity>

}