package project.graduation.vkr.model.data.repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import project.graduation.vkr.model.data.table.OrganizationEntity
import project.graduation.vkr.model.data.table.OrganizationEntity.Companion.TABLE_NAME

@Dao
interface OrganizationRepo : SimpleRepository<OrganizationEntity> {
    @Insert
    override fun insert(elems: Array<OrganizationEntity>): List<Long>

    @Update
    override fun update(elems: Array<OrganizationEntity>): Int

    @Query("select * from $TABLE_NAME where id = :id")
    override fun elem(id: Long): OrganizationEntity

    @Query("select * from $TABLE_NAME")
    override fun elemsLD(): LiveData<List<OrganizationEntity>>

    @Query("select * from $TABLE_NAME")
    override fun elems(): List<OrganizationEntity>

}